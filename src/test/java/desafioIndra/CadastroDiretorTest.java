package desafioIndra;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;


public class CadastroDiretorTest {
    private WebDriver driver;
    private Diretor james;


    @Before
    public void beforeTest(){


        System.setProperty("webDriver.chrome.driver", "C:/Users/tiago.ribeiro/estudoDoZero/chromedriver.exe");
        driver = new ChromeDriver();

        List<Filme> filmesDiretorJames = new ArrayList<>();

        Filme guardioesGalaxia = new Filme("Guardiões das galaxias", "2014");
        filmesDiretorJames.add(guardioesGalaxia);

        Filme guardioesGalaxia2 = new Filme("Guardiões das Galaxias 2","2017");
        filmesDiretorJames.add(guardioesGalaxia2);

        Filme super1 = new Filme("Super", "2010");
        filmesDiretorJames.add(super1);

        Filme paraMaiores = new Filme("Para Maiores", "2013");
        filmesDiretorJames.add(paraMaiores);

        james = new Diretor("James Gunn", "20/02/1980", filmesDiretorJames);




    }

    @Test
    public void test1() throws InterruptedException {
        for (int i = 0; i < james.getFilmeList().size(); i++){
            String url;
            url = "http://www.google.com.br";
            driver.get(url);

            WebElement localPesquisa = driver.findElement(By.name("q"));
            //localPesquisa.sendKeys();

            WebElement logoGoogle = driver.findElement(By.id("hplogo"));
            //logolocalPesquisa.sendKeys()Google.click();

            WebElement botaoPesquisar = driver.findElement(By.cssSelector(".FPdoLc.VlcLAe [name=btnK][type=submit][value='Pesquisa Google']"));
            botaoPesquisar.click();

            WebDriverWait espera = new WebDriverWait(driver, 10);
            espera.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".FPdoLc.VlcLAe [name=btnK][type=submit][value='Pesquisa Google']")));

            localPesquisa.sendKeys(james.getNome()+ " "+ james.getFilmeList().get(i).getNome());
            localPesquisa.sendKeys(Keys.ENTER);
            espera.until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));
            String resultado = driver.findElement(By.id("resultStats")).getText();
            System.out.println(james.getNome() + " - " + james.getFilmeList().get(i).getNome() + " - " + resultado);
            localPesquisa = driver.findElement(By.name("q"));
            localPesquisa.clear();
        }

    }

    @After
    public void tearDown(){
        driver.quit();

    }
}
