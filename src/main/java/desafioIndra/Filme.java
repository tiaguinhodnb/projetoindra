package desafioIndra;

import java.awt.*;

public class Filme {

    private String nome;
    private String anoLancamento;

    public Filme (String nome, String anoLancamento) {
        this.anoLancamento = anoLancamento;
        this.nome = nome;
    }



    public String getNome () {
        return  this.nome;
    }

    public void setNome (String nome) {
         this.nome = nome;
    }


    public String getAnoLancamento (){
        return this.anoLancamento;
    }
    public void setAnoLancamento (String anoLancamento) {
        this.anoLancamento = anoLancamento;
    }


}