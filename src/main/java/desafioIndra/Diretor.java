package desafioIndra;

import java.util.Date;
import java.util.List;

public class Diretor {

    //atributos classe diretor

    private String nome;
    private String dataNascimento;
    private List<Filme> filmeList;


    //Contrutor
    public Diretor(String nome, String dataNascimento, List<Filme> filmeList) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.filmeList = filmeList;
    }


    //getters and setters
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataNascimento() {
        return this.dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public  List <Filme> getFilmeList(){
        return this.filmeList;
    }

    public void setFilmeList (List<Filme> filmeList) {
        this.filmeList = filmeList;
    }

}